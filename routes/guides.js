var express = require('express');
var router = express.Router();
var guide = require("../controllers/GuideController.js");

// Get all guides
router.get('/', function(req, res) {
  // employee.list(req, res);
});

// Get single guide by id
router.get('/show', function(req, res) {
    guide.show(req, function (err, data){
    if (err){
        console.log("err: "+err);
        res.status(500).send({msg: "error in getting guide"})
    }
    else if (!data){
        res.status(500).send({msg: "_doc null"})
    }
    else{
        res.setHeader('Content-Type', 'application/json');
        res.send(data);

    }
  })
});


// Get single guide by id
router.get('/getGuidesList', function(req, res) {
    guide.getGuidesList(req, function (err, data){
        if (err){
            console.log("err: "+err);
            res.status(500).send({msg: "error in getting guide"})
        }
        else if (!data){
            res.status(500).send({msg: "_doc null"})
        }
        else{
            res.setHeader('Content-Type', 'application/json');
            res.send(data);

        }
    })
});


// Create guide
router.get('/create', function(req, res) {
    guide.create(req, res);
});

// Save guide
router.post('/save', function(req, res) {
    guide.save(req, function (err){
    if (err){
      res.status(500).send({msg: "error in saving new"})
    }
    else{
      res.setHeader('Content-Type', 'application/json');
      res.send("success");
    }
  });
});

// delete update
router.post('/delete/:id', function(req, res, next) {
    guide.delete(req, res);
});

module.exports = router;
