var express = require('express');
var router = express.Router();
var user = require("../controllers/userController.js");


/* GET users listing. */
router.get('/', function(req, res, next) {
  res.send('respond with a resource');
});

// Save guide
router.post('/register', function(req, res) {
    user.save(req, function (err, data){
        if (err){
            if (err.errmsg.includes('duplicate key')){
                res.setHeader('Content-Type', 'application/json');
                res.send({text: "שם המשתמש קיים, בחר שם משתמש אחר"});
            }
            else{
                res.status(500).send({msg: "error in saving new"})
            }
        }
        else{
            res.setHeader('Content-Type', 'application/json');
            res.send({text:"success"});
        }
    });
});

router.get('/login', function(req, res) {
    user.login(req, function (err, data){
        if (err){
            res.status(500).send({msg: "error in login"})
        }
        else{
            res.setHeader('Content-Type', 'application/json');
            res.send(data);
        }
    });
});




module.exports = router;
