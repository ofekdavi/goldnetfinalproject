var expect = require("chai").expect;
var tools = require("../lib/tools");

describe("printName()", function() {

	it("should print the last name first", function(){
		var result = tools.printName({ first: "Sivan", last: "tt"});
		expect(result).to.equal("tt, Sivan");
	});
});
