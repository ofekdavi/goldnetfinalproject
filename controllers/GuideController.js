var mongoose = require("mongoose");
var Guide = require("../models/Guide");

var guideController = {};

// Show list of guides
guideController.list = function(req, res) {
    Guide.find({}).exec(function (err, guides) {
    if (err) {
      console.log("Error:", err);
    }
    else {
      res.render("../views/guides/index", {guides: guides});
    }
  });
};

//todo: fixed query and instead of Guide. go the relevant collection
guideController.getGuidesList = function (req,callback){
    var username = req.query.username;
    var query = {username: username, step_in_guide: 0};
    Guide.find(query).exec(function (err,guidesList){
        if (err) {
            console.log("Error:", err);
            callback(err,null);
        }
        else {
            console.log("found user guides' list");
            callback(null,guidesList);
        }
    })
}

// Show guide by id
guideController.show = function(req, callback) {
    console.log(req.query.guideName);
    // if (req.params.id === 'bus'){
        // Guide.findOne({url: {$regex: ".*rail.co.il.*"}}).exec(function (err, guide) {
        // Guide.findOne({url: {$regex: ".*132.72.23.64.*"}}).exec(function (err, guide) {
        // var query = { guide_name: req.query.guideName, step_in_guide: req.query.step};
        // Guide.findOne(query).exec(function (err, guide) {
        var query = { guide_name: req.query.guideName};
        Guide.find(query).exec(function (err, guides) {
            if (err) {
                console.log("Error:", err);
                callback(err,null);
            }
            else {
                //res.render("../views/employees/show", {employee: employee});
                console.log("found bus guide");
                callback(null,guides);
            }
        });
    // }

    if (req.params.id === 'button'){
        // Guide.findOne({url: {$regex: ".*rail.co.il.*"}}).exec(function (err, guide) {
        Guide.findOne({action_name: {$regex: "button"}}).exec(function (err, guide) {

            if (err) {
                console.log("Error:", err);
                callback(err,null);
            }
            else {
                //res.render("../views/employees/show", {employee: employee});
                console.log("found bus guide");
                callback(null,guide);
            }
        });
    }

    if (req.params.id === 'text'){
        // Guide.findOne({url: {$regex: ".*rail.co.il.*"}}).exec(function (err, guide) {
        Guide.findOne({action_name: {$regex: "text"}}).exec(function (err, guide) {

            if (err) {
                console.log("Error:", err);
                callback(err,null);
            }
            else {
                //res.render("../views/employees/show", {employee: employee});
                console.log("found bus guide");
                callback(null,guide);
            }
        });
    }



};

// Show guide by id
/*guideController.show = function(req, callback) {
    if (req.params.id === 'button'){
        // Guide.findOne({url: {$regex: ".*rail.co.il.*"}}).exec(function (err, guide) {
        Guide.findOne({action_name: {$regex: "button"}}).exec(function (err, guide) {

            if (err) {
                console.log("Error:", err);
                callback(err,null);
            }
            else {
                //res.render("../views/employees/show", {employee: employee});
                console.log("found bus guide");
                callback(null,guide);
            }
        });
    }

};*/

// Show guide by id
/*guideController.show = function(req, callback) {
    if (req.params.id === 'text'){
        // Guide.findOne({url: {$regex: ".*rail.co.il.*"}}).exec(function (err, guide) {
        Guide.findOne({action_id: {$regex: "800"}}).exec(function (err, guide) {

            if (err) {
                console.log("Error:", err);
                callback(err,null);
            }
            else {
                //res.render("../views/employees/show", {employee: employee});
                console.log("found bus guide");
                callback(null,guide);
            }
        });
    }

};*/

// Create new guide
guideController.create = function(req, res) {
  res.render("../public/create.html");
};

// Save new guide
guideController.save = function(req, callback) {
  var guide = new Guide(req.body);

    guide.save(function(err) {
    if(err) {
      console.log(err);
      callback(err);
    } else {
      console.log("Successfully created a guide.");
      callback(null); //success
    }
  });
};

// Edit a guide
guideController.edit = function(req, res) {
    Guide.findOne({_id: req.params.id}).exec(function (err, guide) {
    if (err) {
      console.log("Error:", err);
    }
    else {
      res.render("../views/guides/edit", {guide: guide});
    }
  });
};

// Update a guide
guideController.update = function(req, res) {
  Guide.findByIdAndUpdate(req.params.id, { $set: { action_name: req.body.action_name, action_id: req.body.action_id, url: req.body.url}}, { new: true }, function (err, guide) {
    if (err) {
      console.log(err);
      res.render("../views/guides/edit", {guide: req.body});
    }
    res.redirect("/guides/show/"+guide._id);
  });
};

// Delete a guide
guideController.delete = function(req, res) {
    Guide.remove({_id: req.params.id}, function(err) {
    if(err) {
      console.log(err);
    }
    else {
      console.log("guide deleted!");
      res.redirect("/guides");
    }
  });
};

module.exports = guideController;
