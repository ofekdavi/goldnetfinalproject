/**
 * Created by user on 4/24/2018.
 */
var mongoose = require("mongoose");
var User = require("../models/User");

var userController = {};

// Show list of users
userController.list = function(req, res) {
    User.find({}).exec(function (err, guides) {
        if (err) {
            console.log("Error:", err);
        }
        else {
            res.render("../views/users/index", {users: users});
        }
    });
};


userController.login= function(req, callback) {
    var username = req.query.username;
    var password = req.query.password;
    User.findOne({"username": username, "password": password}).exec(function (err, user) {
        if (err) {
            console.log("Error:", err);
            callback(null, {"isLoggedIn": false});
        }
        else if (user === null) {
            console.log("Error:", "no such user");
            callback(null, {"isLoggedIn": false});
        }
        else{
            callback(null, {"isLoggedIn": true});
        }
    });
};


// Show guide by id
userController.show = function(req, callback) {

};



// Create new guide
userController.create = function(req, res) {
    res.render("../public/create.html");
};

// Save new guide
userController.save = function(req, callback) {
    var user = new User(req.body.params);

    user.save(function(err) {
        if(err) {
            console.log(err);
            callback(err);
        } else {
            console.log("Successfully created a guide.");
            callback(null); //success
        }
    });
};

// Edit a user
userController.edit = function(req, res) {
    User.findOne({_id: req.params.id}).exec(function (err, guide) {
        if (err) {
            console.log("Error:", err);
        }
        else {
            res.render("../views/guides/edit", {guide: guide});
        }
    });
};

// Update a guide
userController.update = function(req, res) {
    User.findByIdAndUpdate(req.params.id, { $set: { action_name: req.body.action_name, action_id: req.body.action_id, url: req.body.url}}, { new: true }, function (err, guide) {
        if (err) {
            console.log(err);
            res.render("../views/guides/edit", {guide: req.body});
        }
        res.redirect("/guides/show/"+guide._id);
    });
};

// Delete a user
userController.delete = function(req, res) {
    User.remove({_id: req.params.id}, function(err) {
        if(err) {
            console.log(err);
        }
        else {
            console.log("user deleted!");
            res.redirect("/guides");
        }
    });
};

module.exports = userController;
