/**
 * Created by user on 4/24/2018.
 */
/**
 * Created by user on 4/2/2018.
 */
/**
 * Created by user on 3/11/2018.
 */


angular.module('goldNet', []).controller('registerCtrl', function($scope, $http) {

    $scope.username = "";
    $scope.password = "";
    $scope.errorMessage="";

    $scope.register = function () {
        var userName = $scope.username;
        var password = $scope.password;
        var url = 'http://127.0.0.1:4000/users/register';
        $http.post(url, {params: {username: userName, password: password}}).then(function (response) {
            debugger;
            if (response.data.text === 'success') { //todo: good! navigate to myGuides or create guides (older or young)

                chrome.storage.sync.set({"username": userName}, function (){
                    window.location = 'navigation.html';
                })

            }
            else {
                $scope.errorMessage = response.data.text;
                $scope.username = "";
                $scope.password = "";
            }
        })
    }

});


