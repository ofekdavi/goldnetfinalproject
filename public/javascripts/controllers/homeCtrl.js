/**
 * Created by user on 4/2/2018.
 */
/**
 * Created by user on 3/11/2018.
 */


angular.module('goldNet', []).controller('homeCtrl', function($scope, $http) {

    $scope.username = "";
    $scope.password = "";
    $scope.errorMessage="";

    $scope.loginClicked = function (){
        var userName = $scope.username;
        var password = $scope.password;
        var url = 'http://127.0.0.1:4000/users/login';
        $http.get(url, {params: {username: userName, password: password}}).then(function (response){
            if (response.data.isLoggedIn){ //todo: good! navigate to myGuides or create guides (older or young)
                chrome.storage.sync.set({"username": userName}, function (){
                    window.location = 'navigation.html';
                })
            }
            else{
                $scope.errorMessage = "פרטי התחברות שגויים"
                $scope.username = "";
                $scope.password = "";
            }
        })
        // window.location = 'navigation.html';
    }

    $scope.registerClicked = function (){
        window.location = 'register.html';
    }

});


