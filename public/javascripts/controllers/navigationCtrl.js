/**
 * Created by user on 4/2/2018.
 */

angular.module('goldNet', []).controller('navigationCtrl', function($scope, $http) {

    $scope.watchMyGuides = function (user){
        chrome.storage.sync.set({"is_running": true, "correct_step": true}, function () {
            window.location = 'displayGuides.html';
            console.log("in_running changed to true, correct step changed to false");
        })

    }

    $scope.manageGuides = function (user){
        chrome.storage.sync.set({"is_running": false, "correct_step": false}, function () {
            window.location = 'manageGuides.html';
            console.log("in_running changed to true, correct step changed to false");
        })

    }

});


