/**
 * Created by ofek on 12/23/2017.
 */
var portConToBg = chrome.extension.connect({name: "contentToBG"});
var portPopupToBg = chrome.extension.connect({name: "portPopupToBg "});
var stepCounter = 0;
var textOfStep = "";

chrome.storage.sync.set({'canConnectToContent': false}, function () {
    console.log('changes canConnectToContent to false in storage');
});

function add() {
    chrome.storage.sync.get("stepsCounter", function (counter) {
        chrome.storage.sync.set({'stepsCounter': counter + 1}, function () {
            stepCounter++;
            console.log('counter++');
        });
    })
}


chrome.runtime.onConnect.addListener(function (port) {
    port.onMessage.addListener(function (msgFromContent) {

        console.log("got msg from content page");

        chrome.storage.sync.get("currentGuide", function (currGuide) {
            console.log(currGuide);
            //debugger;
            chrome.storage.sync.get("username", function (user){
                console.log(user);
                //debugger;
                var msg = JSON.parse(msgFromContent);
                if (msg.clicked && msg.location) { //a button has clicked
                    chrome.storage.sync.get("user_text", function (userText) {
                        console.log("try to take user text from storage")
                        console.log(userText.user_text)
                        /*if (userText.user_text.length > 0) {
                         textOfStep = userText.user_text;
                         console.log(textOfStep);
                         }*/
                        add();
                        var xhr = new XMLHttpRequest();
                        //xhr.open('POST', 'http://132.72.23.64:4000/guides/save', true);
                        xhr.open('POST', 'http://127.0.0.1:4000/guides/save', true);

                        // xhr.open('POST', 'http://35.200.171.136:80/guides/save', true);
                        xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
                        var name;

                        chrome.storage.sync.get("button_or_input", function (buttonOrInput) {
                            console.log("try to take name from storage")
                            name = buttonOrInput.button_or_input;

                            var address = msg.clicked;


                            var req = 'type=createGuide&action_name=' + name + '&action_id=' + address + '&url=' + msg.location + '&step_in_guide=' + stepCounter + '&user_text=' + userText.user_text + '&guide_name=' + currGuide.currentGuide + '&username=' + user.username;


                            xhr.send(req);

                            xhr.onreadystatechange = processRequest;

                            function processRequest(e) {
                                if (xhr.readyState == XMLHttpRequest.DONE && xhr.status == 200) {
                                    var response = xhr.responseText;

                                }
                                if (response == 'failed') {
                                    console.log('failedddddddddddd')
                                }
                                if (response && response.length > 0) {
                                    console.log(response)
                                }


                            }
                        })
                    })
                    chrome.storage.sync.set({'user_text': ""}, function () {})

                }
        })
       })
    });
});