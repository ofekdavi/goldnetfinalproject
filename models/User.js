/**
 * Created by user on 4/24/2018.
 */
var mongoose = require('mongoose');

var UserSchema = new mongoose.Schema({
    username: {type: String,required: true, default: "default", unique: true},
    password: {type: String, required: true, default: 0}
});

module.exports = mongoose.model('User', UserSchema);
