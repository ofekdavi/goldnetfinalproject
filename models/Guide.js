var mongoose = require('mongoose');

var GuideSchema = new mongoose.Schema({
  action_name: String,
  action_id: String,
  url: String,
  user_text: String,
  updated_at: { type: Date, default: Date.now },
  guide_name: {type: String,required: true, default: "default"},
  step_in_guide: {type: Number, required: true, default: 0},
  username: {type: String, required: true},
  text_to_display: String
});

module.exports = mongoose.model('Guide', GuideSchema);
